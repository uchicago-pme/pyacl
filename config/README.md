## About the folders.json file

This file lists the key:value pairs for the folders that will be managed by
an access control list. 

## Managing ACLs for folders 

Each folder to be actively managed and all of its settings are contained within
the curly brace list element in the `folders.json` file. If a folder is listed
in the `folders.json` file that does not exist, it will be created and the ACL 
permissions specified applied. 

Note that you can only set ACL on folders for which you are the owner. 

```bash
{
  "name"   : "example1",
  "path"   : "/project2/rcc/sandbox",
  "shared" : "false",
  "users"  : [
     {      "user":"jhskone",
            "permissions":"rwx"
     },
     {      "user":"mxcheng",
            "permissions":"rwx"
     }
            ]
},
```

### Keys 
* "name"   <type: string> The name of the folder that will be managed  
* "path"   <type: string> The absolute path to the folder on the file system
* "shared" <type: logical> Indicates whether or not the group ID is kept
* "users"  <type: list> A list of user CNetIDs and their permissions
           Permissions can only be 'r' read,  'w' write, and or 'x' execute