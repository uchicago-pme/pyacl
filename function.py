""" Set of suporting utilities."""

import os
import sys
import math
from datetime import datetime,timedelta

def shexec(args):
   """ Execute shell command for all arguments passed
   
   :param args: set of arguments to execute from bash shell
   :type args: string

   """
   cmdline = " ".join(args)
   #print cmdline;
   p = os.popen(cmdline, "r")
   lines = p.read().splitlines()
   p.close()
   if lines is None: lines = []
   return lines


def Pad(text, length, char=' '):

    slen = abs(length)
    sign = length - slen

    if len(text) <= slen :
        for i in range(slen-len(text)):
            if sign < 0 : text = char + text
            else : text += char

    else :
        len1 = int(math.floor(slen/2))
        text = text[0:len1-1] + '...' + text[2-slen+len1:-1] + ' '

    return text


def today():
    """ Return today's date.

    :args: None 
    :returns: today's date in format YY-MM-DD
    :rtype: string
    
    """ 
    return datetime.now().strftime("%Y-%m-%d")
