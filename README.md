# pyacl

python tool for managing a folder's access control list (acl). 
The tool can be used by any user on any folder for which the user is the owner. 

## Setup

Clone this repository to a location on midway (preferably either
/home/<user> or /project2/\<pi-group\>/\<user\>). 

There are two files that require your attention: 
* **settings.py** :  python file that sets the `config_path`
   The `config_path` should be set to the location where  you will keep the folders
   `json file`. The default location is in the `config` folder. If this location 
   is satisfactory, then you do not need to mofidy the settings.py `config_path`. 
* **folders.json** : file containing all subfolders to manage with user permissions.
   See the [config README](config/README.md) file for more information on 
   setting up the folder permissions file. 

You then should souce the `env.sh` script. You only ever need to do this once. 
```bash
source env.sh
```
This will add the pyacl.py command to your PATH so
that it can be executed from anywhere on the file system. It will also add the 
alias `pyacl` and enables user execute permissions on the pyacl.py file. 

## Usage

### Listing ACLs on folders
To list the folders under active ACL management, issue the following from the CLI:
```bash
pyacl list 
```

### Updating the ACLs on folders
To update the folders under ACL management, you need to first update the
[folders.json](config/folders.json) file with the folders and user permissions
that should be added or removed. You may create a new folder if it does not exist 
or apply ACL to an existing folder that you own. You can NOT apply ACL to a folder 
for which you are not an owner. To update the folders that should have ACL applied,
issue the following from the CLI:
```bash
pyacl update
```