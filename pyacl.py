#!/usr/bin/python

""" python tool for managing folder acls."""

import os
import sys
import argparse
import json
#from docopt import docopt
 
filepath = os.path.dirname(os.path.abspath(__file__))
from settings import settings
from function import *

#
# define acl functions update and list
#
def acl_update(dirs):
    """ update the acls on the folders specified """
    print('updating')
    # Loop over each subfolder/project found in json file
    for subdir in dirs:
        update_subdir(subdir)

def acl_list(dirs):
    """ return the list of acls set on managed folders """
    print('listing')
    print('\n' + "Listing Permissions on Managed Subfolders:")
    for subdir in dirs:
        list_subdir(subdir)

#
# define argument parser commands
#
parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers()

update_parser = subparsers.add_parser('update')
update_parser.set_defaults(func=acl_update)  # set the default function to update
list_parser = subparsers.add_parser('list')
#list_parser.add_argument('name')  # add the name argument if wish to pass arguments after command is specified
list_parser.set_defaults(func=acl_list)  # set the default function to list

def update_subdir(folder):
    """ Updates acl on folder.
    
    :param folder: subfolder key-value pairs for folder update 
    :type folder: dictionary  
    :returns: nothing returned
    :globals: update
    """
    global update
    fullname = folder['path'] + '/' + folder['name']
    new_dir = False
    # 
    # create folders if they do not exist
    #
    if os.path.isdir(fullname) == False :
       new_dir = True
       update  = True
       print('\n' + "Creating Subfolder:  [" + folder['name'] + "]")
       print("Folder Path:         [" + fullname + "]")
       shexec(["mkdir", fullname])
       shexec(["chmod 700", fullname])
       # remove preservation of unix group ownership
       # all files/folders created will be user:user ownership
       # Note even if remove this, anyone w/ group permission 
       # on the folder can still remove the directory if it is empty
       # even if the acl set does not include them and removes group permission
       if  folder['shared'] == "true" : 
           shexec(["chmod g-s", fullname])
    #
    # update users list
    #
    current_users = shexec(["cd", fullname+";" , "getfacl . | awk -F':' '{if($1==\"user\" && $2!=\"\") print $2}'"])
    #
    # add any new users if found in folders.json 
    #
    for user in folder['users']:
#        print(user['user'])
        if user['user'] in current_users : continue
        update  = True
        if new_dir == False : 
           shexec(["chmod 700", fullname])
           print('\n' + "Modifying Subfolder: [" + folder['name'] + "]")
           print("Folder Path:         [" + fullname + "]")
           new_dir = True
        print("Add user:            [%s] " % user['user'])
        print("with permissions:    [%s] " % user['permissions'])
        shexec(["setfacl -m", "u:" + user['user'] + ":" + user['permissions'], fullname])
    #
    # remove any users from a folder if user is no longer listed for a folder in folders.json 
    #
    for user in current_users:
         if [str(folder['users'][index]['user']) for index, (i, s) in enumerate(folder['users']) if folder['users'][index]['user'] == user  ] : continue
         update  = True
         if new_dir == False : 
           print('\n' + "Modifying Subfolder: [" + folder['name'] + "]")
           print("Folder Path:         [" + fullname + "]")
           new_dir = True
         print("Remove user:         [%s] " % user)
#         print("permissions         [%s] " % folder['users'][index]['permissions'] )
         shexec(["setfacl -x", "u:" + user, fullname])

def list_subdir(folder):
    """ List acl on managed folders.
    
    :param folder: subfolder key-value pairs for folder update 
    :type folder: dictionary  
    :returns: nothing returned
    :globals: update
    """
    import os
    global update
    update = True
    fullname = str(folder['path'] + '/' + folder['name'])
#    print(folder['path'], fullname)
    # value = shexec(["getfacl -p ", fullname])
    command = "getfacl -p " + str(fullname)
    os.system(command)
#
# Begin script
#  
if __name__ == '__main__':
    #   
    # Open folders.json and parsing json file
    #
    update  = False
    with open(settings['config_path'] + 'folders.json', 'r') as f:
        sub_dirs = json.load(f)
    #   
    # If run as script parse the CLI arguments
    #
    args = parser.parse_args()
    args.func(sub_dirs)
#   args.func(args) if add list_parser.add_argument('name') need to add 'args' in the function call 
    if update == True : 
       print("\n")
    else: 
       print('\n Nothing to Update \n')

