#
# SETUP PATHS FOR pyacl.py AND pyacl ALIAS SO FOUND GLOBALLY
# pyacl ALIAS IS ADDED TO THE USERS ~./bash_profile
#
CWD=`pwd`
chmod u+x pyacl.py
echo "export PATH=$CWD:\$PATH" >>~/.bash_profile
echo "alias pyacl=pyacl.py" >>~/.bash_profile
source ~/.bash_profile
#
# EOF
#
